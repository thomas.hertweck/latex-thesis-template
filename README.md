## Name
Thesis Template

## Description
A LaTeX template for Bachelor's and Master's theses at the Geophysical 
Institute, KIT.

## Installation
Simply download or clone this repository and start using the template.

The core part is gpithesis.cls, a LaTeX class. It expects the logo to be 
located in the `corefig` subdirectory, but it is easy to change if necessary. 
The class provides a sensible default layout but feel free to modify it if 
you really know what you are doing.

The main LaTeX file is called `thesis.tex`. Some additional `.tex` files to 
simulate various chapters are included as well. Furthermore, a short 
bibliography database `thesis.bib` with three exemplary entries of different
type is provided.

To compile the source, use `pdflatex` or `lualatex` on `thesis.tex`, followed 
by `biber` (to parse the bibliography database) and two additional `pdflatex` 
or `lualatex` runs. If you use overleaf, this will most likely be handled 
automatically. If you compile the source locally, you could use `latexmk` which 
will also automate most of the compilation process. In a nutshell:

```
$> pdflatex thesis && biber thesis && pdflatex thesis && pdflatex thesis
```

## Features
- Sensible default layout that includes matching text and math fonts.
- No-frills keep-it-simple approach.
- Automatically loads many useful LaTeX macro packages required in the context 
of geophysics or physics (like `siunitx` to typset values and units correctly);
see the class file `gpithesis.cls` for details (there are quite a few comments
in said file).
- Automatically typesets the title page using simple commands like 
`\supervisor{}`, `\cosupervisor{}` (mandatory) or `\advisor{}`, `\coadvisor{}`
(optional) in addition to the standard 
commands `\title{}`, `\author{}` and `\date{}`.
- Automatically adjusts logo and other text elements based on the selected
language (German or English); certain commands are even gender-aware (matters 
only for theses in German language).
- Provides a sensible default setup for bibliography settings and adjusts
certain settings where the `biblatex` defaults are sub-optimal for geophysics.
- Provides a simple command to typeset the mandatory statutory declaration.

## Screenshots (example: MSc thesis in English)

![title page](img/title_msc_en.png) ![declaration page](img/decl_msc_en.png)

## Usage
Basically, instead of the standard report or book class, you now use the 
`gpithesis` class. You can specify the usual class options which will be 
passed to the underlying `book` class upon which `gpithesis` is based. 
In addition, there are a few special options for the `gpithesis` class itself:

- `msc` or `bsc` (default: `msc`): type of thesis
- `english` or `german` (default: `english`): language
- `xurl=` (boolean; default: `false`): flag whether to load the `xurl` macro 
package or not. `xurl` can break URLs anywhere which can sometimes be 
beneficial if a lot of URLs are used.
- `parskip=` (boolean; default: `true`): if true, the class will load the macro 
package `parskip` which leads to zero paragraph indentation and an empty line 
between paragraphs, which is often preferred.
- `isodiff=` (boolean; default: `true`): if true, the class loads the `diffcoeff` 
macro package with option `ISO`; this leads to differentials being typeset in 
upright normal font rather than slanted.
- `esvect=` (char; default: `b`): the arrow tip used for vectors by the 
`esvect` macro package.
- `capitalize=` (boolean; default: `false`): if true, capitalized references 
such as "Fig." or "Eq." are used by macro package `cleveref`.
- `abbreviate=` (boolean; default: `true`): if true, references are abbreviated 
by macro package `cleveref`, i.e., they read for instance "eq." rather than 
"equation".

The class uses the standard commands "\title{}", "\author{}" and "\date{}" for 
the thesis title, the student's name and the date of submission. In addition, 
the following commands are defined: 

- `\supervisor{}` and `\cosupervisor{}` - both are mandatory and specify the 
examiner and second examiner, respectively. For documents in German, there are
starred versions of these commands, i.e., `\supervisor*{}` and 
`\cosupervisor*{}` - they should be used if the person is female; an "in" is
then added to, e.g., "Referent" to provide "Referentin" such that a 
gender-correct language is used without a generic "ReferentIn" or "Referent:in".

- The commands `\advisor{}` and `\coadvisor{}` are optional. They are often 
used to mention PostDocs or external people involved in overseeing the thesis 
project. Again, there are starred versions `\advisor*{}` and `\coadvisor*{}`
available for documents in German (see explanation above).

- The command `\declaration` will output the statutory declaration that needs 
to be part of all theses. 

Otherwise, use standard LaTeX commands or commands provided by the loaded 
macro packages to write your thesis. If possible, avoid to "manipulate" or 
"override" LaTeX settings and rules. At the end of the day, the standard LaTeX 
settings often provide a superior solution; it's a document preparation system 
after all.

## Support
If technical issues are encountered when using this template, or if there are 
suggestions or ideas for improvement, you can get in touch with the author 
at Thomas.Hertweck@kit.edu.

## License
(c)2023-2024. This work is licensed under a CC BY-NC 4.0 license.
