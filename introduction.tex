\chapter{Introduction}

\section*{Mathematics / Physics}
\begin{itemize}
\item Package \href{https://ctan.org/pkg/amsmath}{\texttt{amsmath}} for mathematical features.
\item Package \href{https://ctan.org/pkg/mathtools}{\texttt{mathtools}} to enhance the appearance of documents containing a lot of mathematics. 
\item Package \href{https://ctan.org/pkg/diffcoeff}{\texttt{diffcoeff}} to set differential coefficients (ordinary, partial and mixed partial derivatives) easily and consistently. Class option determines whether ``ISO'' option is used for this package (default: yes).
\item Package \href{https://ctan.org/pkg/esvect}{\texttt{esvect}} to typeset vectors correctly, in particular vectors with indices. Class option determines what type of arrow tip is used (default: b).
\item Package \href{https://ctan.org/pkg/siunitx}{\texttt{siunitx}} to typeset units correctly in both text and math mode.
\end{itemize}

\section*{Fonts / Layout}
\begin{itemize}
\item Package \href{https://ctan.org/pkg/textcomp}{\texttt{texcomp}} for special text symbols.
\item Package \href{https://ctan.org/pkg/amsfonts}{\texttt{amssymb}} for special mathematical symbols.
\item Package \href{https://ctan.org/pkg/geometry}{\texttt{geometry}} to simplify the set-up of a sensible page layout.
\item Package \href{https://ctan.org/pkg/fancyhdr}{\texttt{fancyhdr}} to control page headers and footers.
\item Package \href{https://ctan.org/pkg/parskip}{\texttt{parskip}} to set layout with zero \texttt{\textbackslash{}parindent} and non-zero \texttt{\textbackslash{}parskip} (i.e., no paragraph indentation but paragraphs separated by an empty line instead). Class option determines whether this package gets loaded (default: yes).
\item Package \href{https://ctan.org/pkg/emptypage}{\texttt{emptypage}} to prevent page numbers and headings on automatically inserted empty pages.
\item Package \href{https://ctan.org/pkg/nextpage}{\texttt{nextpage}} to provide additional page advance commands.
\item Package \href{https://ctan.org/pkg/tocbibind}{\texttt{tocbibind}} to automatically add LoF, LoT and bibliography to table of contents. (Note: table of contents itself is not included in ToC.)
\item Package \href{https://ctan.org/pkg/csquotes}{\texttt{csquotes}} to offer context-sensitive quotation facilities.
\item Package \href{https://ctan.org/pkg/caption}{\texttt{caption}} to customize captions in floating environments (figures, tables).
\item Package \href{https://ctan.org/pkg/listings}{\texttt{listings}} to typeset source code in \LaTeX\@.
\item Package \href{https://ctan.org/pkg/enumitem}{\texttt{enumitem}} to control layout of ``itemize'' and ``enumerate'' environments.
\item Package \href{https://ctan.org/pkg/titlesec}{\texttt{titlesec}} to control layout of titles.
\item Package \href{https://ctan.org/pkg/rotating}{\texttt{rotating}} to provide rotation tools, including rotated full-page floats.
\item Package \href{https://ctan.org/pkg/booktabs}{\texttt{booktabs}} to set publication-quality tables in \LaTeX\@. An alternative package is the relatively new \href{https://ctan.org/pkg/tabularray}{\texttt{tabularray}}.
\item Package \href{https://ctan.org/pkg/multirow}{\texttt{multirow}} to create tabular cells spanning multiple rows.
\item Package \href{https://ctan.org/pkg/adjustbox}{\texttt{adjustbox}} to adjust boxed content.
\end{itemize}

\section*{Graphics}
\begin{itemize}
\item Package \href{https://ctan.org/pkg/xcolor}{\texttt{xcolor}} for driver-independent color extensions.
\item Package \href{https://ctan.org/pkg/graphicx}{\texttt{graphicx}} to include external graphics files in PDF, PNG or JPG format (formats to be preferred in this order; always try to use a vector graphics format).
\item Package \href{https://ctan.org/pkg/wrapfig}{\texttt{wrapfig}} to allow text flowing around figures. Alternative packages are \href{https://ctan.org/pkg/floatflt}{\texttt{floatflt}}, \href{https://ctan.org/pkg/picins}{\texttt{picins}} and \href{https://ctan.org/pkg/picinpar}{\texttt{picinpar}}. (Attention: all packages are rather old; better avoid.)
\item Package \href{https://ctan.org/pkg/placeins}{\texttt{placeins}} to stop floating environments from passing beyond a certain point.
\item Package \href{https://ctan.org/pkg/subcaption}{\texttt{subcaption}} to set captions for subfigures.
\item Package \href{https://ctan.org/pkg/tikz}{\texttt{tikz}} to create high-quality vector graphics in \LaTeX\@. (Note: you have to load the various TikZ libraries yourself.)
\item Package \href{https://ctan.org/pkg/pgfplots}{\texttt{pgfplots}} to simplify the creation of mathematical function plots in two and three dimensions.
\end{itemize}

\section*{References and URLs}
\begin{itemize}
\item Package  \href{https://ctan.org/pkg/xurl}{\texttt{xurl}} to allow URL breaks at any alphanumerical character. Class option determines whether this package gets loaded (default: no). (Note: package only useful if you deal with many URLs.)
\item Package \href{https://ctan.org/pkg/biblatex}{\texttt{biblatex}} to handle sophisticated bibliographies in \LaTeX\@.
\item Package \href{https://ctan.org/pkg/varioref}{\texttt{varioref}} to offer intelligent page references.
\item Package \href{https://ctan.org/pkg/cleveref}{\texttt{cleveref}} to offer intelligent cross-referencing in general (automatic detection of references to figures or formulas). The class options \texttt{capitalize} and \texttt{abbreviate} control some of the \texttt{cleveref} formatting options.
\item Package \href{https://ctan.org/pkg/hyperref}{\texttt{hyperref}} to offer extensive support for hypertext (links within the document and to external web sites). Note that the ugly frames created by \texttt{hyperref}'s default settings have been turned off -- this does not affect the functionality of links.
\end{itemize}

\section*{Miscellaneous}
\begin{itemize}
\item Package \href{https://ctan.org/pkg/babel}{\texttt{babel}} to offer multilingual support (typographical rules for a wide range of languages).
\item Package \href{https://ctan.org/pkg/xspace}{\texttt{xspace}} to define commands that appear not to eat spaces. (Note: only useful if you define your own macros.)
\item Package \href{https://ctan.org/pkg/etoolbox}{\texttt{etoolbox}} to offer a toolbox of programming facilities geared towards the advanced user. (Note: only useful if you define your own \LaTeX{} macros etc.)
\item Package \href{https://ctan.org/pkg/xifthen}{\texttt{xifthen}} to offer extended conditional commands. (Note: only useful if you define your own macros.)
\item Package \href{https://ctan.org/pkg/afterpage}{\texttt{afterpage}} to execute a command after the next page break.
\item Package \href{https://ctan.org/pkg/fontenc}{\texttt{fontenc}} to select font encodings. No user input required.
\item Package \href{https://ctan.org/pkg/inputenc}{\texttt{inputenc}} to accept different input encodings. No user input required.
Package \href{https://ctan.org/pkg/microtype}{\texttt{microtype}} for subliminal refinements towards typographical perfection. No user input required.
\end{itemize}

