%% gpithesis.cls: generic LaTeX class for BSc/MSc theses at the Geophysical Institute, KIT
%% =======================================================================================
%% original author: Dr. Thomas Hertweck, Thomas.Hertweck@kit.edu
%% v1.0: initial release
%% v1.1: update to support BSc theses; update to support theses in German language
%% =======================================================================================

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{gpithesis}[2024/01/23 v1.1 GPI BSc/MSc Thesis]

\RequirePackage{kvoptions}
\RequirePackage[l2tabu,orthodox]{nag}
\RequirePackage{xifthen}
\RequirePackage{ifmtarg}
\RequirePackage{iftex}

% class options setup
\SetupKeyvalOptions{
  family = GPI,
  prefix = gpi@
}

% language settings
\DeclareBoolOption[false]{german}
\DeclareComplementaryOption{english}{german}

% Master's thesis or Bachelor's thesis
\DeclareBoolOption[true]{msc}
\DeclareComplementaryOption{bsc}{msc}

% allow breaking URLs at any character; default: false
\DeclareBoolOption[false]{xurl}

% separate paragraphs by emtpy line; default: true
\DeclareBoolOption[true]{parskip}

% use ISO option for diffcoeff; default: true
\DeclareBoolOption[true]{isodiff}

% arrow tips for esvect package; default: b
\DeclareStringOption[b]{esvect}

% capitalize clever references; default: false
\DeclareBoolOption[false]{capitalize}

% abbreviate clever references; default: true
\DeclareBoolOption[true]{abbreviate}

% parse options
\DeclareDefaultOption{\PassOptionsToClass{\CurrentOption}{book}}
\ProcessKeyvalOptions{GPI}

\ifgpi@german
\ifgpi@msc
  \ClassError{gpithesis}{an MSc thesis at the GPI needs to be in English}{use option 'english' instead of 'german'}
\fi
\fi

% load base class
\LoadClass{book}

% load various macro packages
\RequirePackage[T1]{fontenc}

\ifluatex
\else
\RequirePackage[utf8]{inputenc}
\fi

\RequirePackage{microtype}
\RequirePackage{etoolbox}
\RequirePackage[a4paper,
                width=150mm,
                top=25mm,
                bottom=25mm,
                bindingoffset=6mm]{geometry}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% serif fonts with matching math fonts %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% lmodern
\usepackage{lmodern}
%%%%%%% mlmodern (more black than lmodern)
%\usepackage{mlmodern}
%%%%%%% cm + eufrak
%\usepackage{eufrak}
%%%%%%% lmodern + eufrak
%\usepackage{lmodern} % Latin modern
%\usepackage{eufrak}
%%%%%%% charter
%\usepackage[charter]{mathdesign}
%%%%%%% extended charter + newtxmath
%\usepackage{XCharter}
%\usepackage[uprightscript,charter,vvarbb,scaled=1.05]{newtxmath}
%%%%%%% utopia regular
%\usepackage[utopia]{mathdesign}
%%%%%%% URW Schoolbook L
%\usepackage{fouriernc}
%%%%%%% TeX Gyre Schola
%\usepackage{tgschola}
%%%%%%% fouriernc + newtxmath
%\usepackage{fouriernc}
%\usepackage[libertine]{newtxmath}
%%%%%%% fouriernc + eufrak
%\usepackage{fouriernc}
%\usepackage{eufrak}
%%%%%%% libertine
%\usepackage{libertine}
%\usepackage[libertine]{newtxmath}
%%%%%%% times roman + txmath
%\usepackage{mathptmx}
%%%%%%% times roman + TeX Gyre
%\usepackage{mathptmx,tgtermes}
%\usepackage{eufrak}
%%%%%%% palatino
%\usepackage{newpxtext,newpxmath}
%%%%%%% URW Nimbus Roman
%\usepackage{newtxtext} 
%\usepackage{newtxmath} %% or
%\usepackage[cmintegrals]{newtxmath}
%%%%%%% KP fonts (based on URW Palladio)
%\usepackage{kpfonts}
%%%%%%% Antykwa Torunska
%\usepackage[math]{anttor}
%%%%%%% Concrete + Euler virtual math fonts
%\usepackage{ccfonts,eulervm}
%%%%%%% Fourier-GUTenberg with utopia standard font
%\usepackage[widespace]{fourier}
%%%%%%% palatino + virtual mathpazo fonts
%\usepackage{palatino,mathpazo}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%            sans serif fonts           %%%%%%%
%%%%%%% should not be used in print documents %%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%% Arev (Bitsream Vera Sans)
%\usepackage{arev}
%%%%%%% European Computer Modern Bright
%\usepackage{cmbright}
%%%%%%% Iwona
%\usepackage[math]{iwona}

\RequirePackage{textcomp,amssymb}
\ifgpi@german%
  \RequirePackage[english,main=german]{babel}
  \RequirePackage[german]{isodate}
\else%
  \RequirePackage[german,main=english]{babel}
  \RequirePackage[english]{isodate}
\fi
\RequirePackage{amsmath}
\RequirePackage{mathtools}

\ifgpi@isodiff%
\RequirePackage[ISO]{diffcoeff}
\else%
\RequirePackage{diffcoeff}
\fi

\RequirePackage[\gpi@esvect]{esvect}
\RequirePackage{siunitx}
\RequirePackage[table,svgnames]{xcolor}
\RequirePackage{graphicx}
\RequirePackage{wrapfig}    %%-|
%\RequirePackage{floatflt}  %%-|
%\RequirePackage{picins}    %%-|- alternatives
%\RequirePackage{picinpar}  %%-|
\RequirePackage[section]{placeins}
\RequirePackage{subcaption}
\RequirePackage[autostyle=true]{csquotes}
\RequirePackage{rotating}
\RequirePackage{booktabs}     %%-|
%\RequirePackage{tabularray}  %%-|- alternatives
\RequirePackage{multirow}
\RequirePackage{fancyhdr}
\RequirePackage[backend=biber,
                style=authoryear-comp,
                sorting=nyt,
                sortcites=false,
                maxnames=2,
                minnames=1,
                maxbibnames=10,
                minbibnames=3,
                abbreviate=true,
                doi=true,
                useprefix=true,
                giveninits=true, 
                uniquename=init,
                natbib=true,
                dashed=false]{biblatex}

\ifgpi@xurl%
\RequirePackage[hyphens]{xurl}
\fi

\ifgpi@parskip%
\RequirePackage{parskip}
\fi

\RequirePackage{emptypage}
\RequirePackage{nextpage}
\RequirePackage[nottoc]{tocbibind}
\RequirePackage{xspace}
\RequirePackage{listings}
\RequirePackage{enumitem}
\RequirePackage{titlesec}
\RequirePackage[tableposition=top,
                figureposition=below]{caption}
\RequirePackage{adjustbox}
\RequirePackage{afterpage}
\RequirePackage{tikz,pgfplots}

\ifgpi@abbreviate
\def\gpi@crefabb{}
\else
\def\gpi@crefabb{noabbrev}
\fi

\RequirePackage[nospace]{varioref}                  %%-|
\RequirePackage[hidelinks]{hyperref}                %%-|- do not change order
\ifgpi@capitalize%                                  %%-|
\RequirePackage[capitalize,\gpi@crefabb]{cleveref}  %%-|
\else%                                              %%-|
\RequirePackage[\gpi@crefabb]{cleveref}             %%-|
\fi                                                 %%-|

\urlstyle{same}
\pgfplotsset{compat=newest}
\creflabelformat{equation}{#2\textup{#1}#3}

% additional macros for setting supervisor etc.
% - supervisor and co-supervisor are mandatory
% - advisor and co-advisor are optional
\NewDocumentCommand \supervisor { s m } {%
  \IfBooleanTF{#1}{\gdef\gpi@sv@suffix{in}}{\gdef\gpi@sv@suffix{}}
  \gdef\gpi@supervisor{#2}
}
\def\gpi@supervisor{\@latex@error{No \noexpand\supervisor given}\@ehc}

\NewDocumentCommand \cosupervisor { s m } {%
  \IfBooleanTF{#1}{\gdef\gpi@cosv@suffix{in}}{\gdef\gpi@cosv@suffix{}}
  \gdef\gpi@cosupervisor{#2}
}
\def\gpi@cosupervisor{\@latex@error{No \noexpand\cosupervisor given}\@ehc}

\NewDocumentCommand \advisor { s m } {%
  \IfBooleanTF{#1}{\gdef\gpi@adv@suffix{in}}{\gdef\gpi@adv@suffix{}}
  \gdef\gpi@advisor{#2}
}
\def\gpi@advisor{}

\NewDocumentCommand \coadvisor { s m } {%
  \IfBooleanTF{#1}{\gdef\gpi@coadv@suffix{in}}{\gdef\gpi@coadv@suffix{}}
  \gdef\gpi@coadvisor{#2}
}
\def\gpi@coadvisor{}

\def\gpi@author{\@author}

% biblatex settings
\setlength{\bibitemsep}{0.5\baselineskip}
\DeclareNameAlias{sortname}{family-given}
% remove "In:" from article entries
\renewbibmacro{in:}{%
  \ifentrytype{article}{}{\printtext{\bibstring{in}\intitlepunct}}%
}
% use "et al." instead of "u. a." also in German settings
\DefineBibliographyStrings{german}{
  andothers = {et\addabbrvspace al\adddot},
  andmore   = {et\addabbrvspace al\adddot},
}
% remove thinspace after . for dates in German settings
\DefineBibliographyExtras{german}{% from german.lbx
  \protected\def\mkbibdateshort#1#2#3{%
    \iffieldundef{#3}
    {}
    {\mkdayzeros{\thefield{#3}}\adddot
      \iffieldundef{#2}{}{}}%<-\thinspace removed
    \iffieldundef{#2}
    {}
    {\mkmonthzeros{\thefield{#2}}%
      \iffieldundef{#1}
      {}
      {\iffieldundef{#3}{/}{\adddot}}}%<-\thinspace removed
    \iffieldbibstring{#1}
    {\bibstring{\thefield{#1}}}
    {\dateeraprintpre{#1}\mkyearzeros{\thefield{#1}}}}%
}

% fancyhdr settings
\pagestyle{fancy}
\setlength{\headheight}{14pt}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0pt}
\fancyhf{}
\if@twoside%
\fancyhead[LO,RE]{\textsl{\leftmark}}
\fancyhead[RO,LE]{\textsl{\thepage}}
\fancyhead[C]{}
\else%
\fancyhead[R]{\textsl{\thepage}}
\fancyhead[L]{\textsl{\leftmark}}
\fancyhead[C]{}
\fi%
\fancypagestyle{plain}{%
  \fancyhf{}
\if@twoside%
  \fancyhead[RO,LE]{\textsl{\thepage}}
\else%
  \fancyhead[R]{\textsl{\thepage}}
\fi%
}

\renewcommand{\chaptermark}[1]{%
\markboth{\chaptername\ \thechapter:\ #1}{}}

% language-dependent internal settings
\newif\ifgerman
\ifgpi@german
\germantrue
\else
\germanfalse
\fi

\newcommand{\gpi@bsc@de}{Bachelorarbeit von}
\newcommand{\gpi@bsc@en}{Bachelor's thesis of}
\newcommand{\gpi@msc@de}{Masterarbeit von}
\newcommand{\gpi@msc@en}{Master's thesis of}
\newcommand{\gpi@instname@de}{am Geophysikalischen Institut (GPI)}
\newcommand{\gpi@instname@en}{at the Geophysical Institute (GPI)}
\newcommand{\gpi@depname@de}{KIT-Fakult\"at f\"ur Physik}
\newcommand{\gpi@depname@en}{KIT-Department of Physics}
\newcommand{\gpi@uniname@de}{Karlsruher Institut f\"ur Technologie (KIT)}
\newcommand{\gpi@uniname@en}{Karlsruhe Institute of Technology (KIT)}
\newcommand{\gpi@ref@de}{Referent}
\newcommand{\gpi@ref@en}{Supervisor}
\newcommand{\gpi@coref@de}{Korreferent}
\newcommand{\gpi@coref@en}{Co-supervisor}
\newcommand{\gpi@adv@de}{Betreuer}
\newcommand{\gpi@adv@en}{Advisor}
\newcommand{\gpi@coadv@de}{Co-Betreuer}
\newcommand{\gpi@coadv@en}{Co-advisor}
\newcommand{\gpi@date@de}{Datum der Abgabe:}
\newcommand{\gpi@date@en}{Date of submission:}
\newcommand{\gpi@sig@de}{Unterschrift:}
\newcommand{\gpi@sig@en}{Signature:}

\newcommand{\gpi@type}{\@empty}
\newcommand{\gpi@instname}{\@empty}
\newcommand{\gpi@depname}{\@empty}
\newcommand{\gpi@uniname}{\@empty}
\newcommand{\gpi@ref}{\@empty}
\newcommand{\gpi@coref}{\@empty}
\newcommand{\gpi@adv}{\@empty}
\newcommand{\gpi@coadv}{\@empty}
\newcommand{\gpi@date}{\@empty}
\newcommand{\gpi@sig}{\@empty}
\newcommand{\gpi@unilogo@file}{\@empty}
\ifgpi@german
  \ifgpi@msc
  \renewcommand{\gpi@type}{\gpi@msc@de\relax}
  \else
  \renewcommand{\gpi@type}{\gpi@bsc@de\relax}
  \fi
  \renewcommand{\gpi@instname}{\gpi@instname@de\relax}
  \renewcommand{\gpi@depname}{\gpi@depname@de\relax}
  \renewcommand{\gpi@uniname}{\gpi@uniname@de\relax}
  \renewcommand{\gpi@ref}{\gpi@ref@de\gpi@sv@suffix\relax}
  \renewcommand{\gpi@coref}{\gpi@coref@de\gpi@cosv@suffix\relax}
  \renewcommand{\gpi@adv}{\gpi@adv@de\gpi@adv@suffix\relax}
  \renewcommand{\gpi@coadv}{\gpi@coadv@de\gpi@coadv@suffix\relax}
  \renewcommand{\gpi@date}{\gpi@date@de\relax}
  \renewcommand{\gpi@sig}{\gpi@sig@de\relax}
  \renewcommand{\gpi@unilogo@file}{corefig/kit_logo_de}
\else
  \ifgpi@msc
  \renewcommand{\gpi@type}{\gpi@msc@en\relax}
  \else
  \renewcommand{\gpi@type}{\gpi@bsc@en\relax}
  \fi
  \renewcommand{\gpi@instname}{\gpi@instname@en\relax}
  \renewcommand{\gpi@depname}{\gpi@depname@en\relax}
  \renewcommand{\gpi@uniname}{\gpi@uniname@en\relax}
  \renewcommand{\gpi@ref}{\gpi@ref@en\relax}
  \renewcommand{\gpi@coref}{\gpi@coref@en\relax}
  \renewcommand{\gpi@adv}{\gpi@adv@en\relax}
  \renewcommand{\gpi@coadv}{\gpi@coadv@en\relax}
  \renewcommand{\gpi@date}{\gpi@date@en\relax}
  \renewcommand{\gpi@sig}{\gpi@sig@en\relax}
  \renewcommand{\gpi@unilogo@file}{corefig/kit_logo_en}
\fi

% title page
\renewcommand*{\maketitle}{%
\ifdefempty{\gpi@advisor}{%
\ifdefempty{\gpi@coadvisor}{}{%
\ClassError{gpithesis}{no advisor given but co-advisor}{use advisor instead of co-advisor}}}{}%
\begin{titlepage}%
\centering%
\vspace*{5mm}%
\includegraphics[width=0.3\textwidth]{\gpi@unilogo@file}\par%
\vfill%
{\Huge\bfseries\@title\unskip\strut\par}%
\vfill%
{\Large \gpi@type}\\[2\baselineskip]%
{\Large\textbf{\@author}\unskip\strut}\\[2\baselineskip]%
{\Large \gpi@instname}\\[0.5\baselineskip]%
{\Large \gpi@depname}\\[0.5\baselineskip]%
{\Large \gpi@uniname}\\%
\vfill%
{\Large \gpi@date}\\[0.5\baselineskip]%
{\Large {\@date}\unskip\strut}\\%
\vfill%
{\Large%
\begin{tabular}{ll}%
\gpi@ref:~ & ~\gpi@supervisor\unskip\strut \\[0.5\baselineskip]%
\gpi@coref:~ & ~\gpi@cosupervisor\unskip\strut   \\[0.5\baselineskip]%
\ifthenelse{\equal{\gpi@advisor}{}}{}{%
\gpi@adv:~ & ~\gpi@advisor\unskip\strut \\[0.5\baselineskip]%
}%
\ifthenelse{\equal{\gpi@coadvisor}{}}{}{%
\gpi@coadv:~ & ~\gpi@coadvisor\unskip\strut   \\[0.5\baselineskip]%
}%
\end{tabular}%
\vspace{10mm}%
}%
\end{titlepage}%
}

% enumitem settings
\setlist[itemize]{noitemsep}
\setlist[enumerate]{noitemsep}

% caption settings
\captionsetup{format=plain,
              labelformat=default,
              labelfont={bf},
              font={it,normalsize},
              labelsep=colon,
              justification=justified,
              singlelinecheck=true,
              margin=0pt}

% additional commands
\DeclareDocumentCommand \declaration { } {%
\ifgpi@german%
\chapter{Erkl\"arung}
Ich versichere wahrheitsgem\"a\ss{}, die Arbeit selbstst\"andig verfasst,
alle benutzten Hilfsmittel vollst\"andig und genau angegeben und alles
kenntlich gemacht zu haben, was aus Arbeiten anderer unver\"andert oder
mit Ab\"anderungen entnommen wurde sowie die Satzung des KIT zur Sicherung
guter wissenschaftlicher Praxis in der momentan g\"ultigen Fassung beachtet
zu haben.\\[2\baselineskip]
\else%
\chapter{Erkl\"arung / Statutory declaration}
Ich versichere wahrheitsgem\"a\ss{}, die Arbeit selbstst\"andig verfasst,
alle benutzten Hilfsmittel vollst\"andig und genau angegeben und alles
kenntlich gemacht zu haben, was aus Arbeiten anderer unver\"andert oder
mit Ab\"anderungen entnommen wurde sowie die Satzung des KIT zur Sicherung
guter wissenschaftlicher Praxis in der momentan g\"ultigen Fassung beachtet
zu haben.\\[\baselineskip]
I declare truthfully that I have written this thesis all by myself, that I have
fully and accurately specified all auxiliary means used, that I have correctly
cited (marked) everything that was taken, either unchanged or with modification,
from the work of others, and that I have complied with the current version of
the KIT statutes for safeguarding good scientific practice.\\[2\baselineskip]
\fi%
\begin{center}
\begin{tabular}{lcc}
Karlsruhe, \@date & \hspace*{1cm} & \hspace*{8cm} \\ \cmidrule{3-3}
 & & \gpi@sig~\gpi@author\\
\end{tabular} 
\end{center}
}
